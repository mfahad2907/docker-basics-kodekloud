# Docker
‏‏‎ ‎
##  ❯ Getting Started

### ❯ Installing Dcoker
To install docker visit their page https://docs.docker.com/engine/install/ and search for the desired OS.  
Scroll Down to ```Install using the convenience script``` then run the commands below.  
!! For Ubuntu !!
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```
---
### ❯ Verifying Installation
To verify if we installed docker correctly we can run the following image from https://hub.docker.com/
https://hub.docker.com/r/docker/whalesay  
We can run this image with the following command.
```
docker run docker/whalesay cowsay boo
```
Note : "boo" is a messaged that is displayed which can be changed to anything. e.g "Hello-World"

---
‏‏‎ ‎
##  ❯ Docker Commands

### ❯ run - start a container

The run commands run any app inside a container.
E.g The following run commands run nginx.  
```
docker run nginx
```
---
### ❯ ps - list containers and their info

Lists all runnig containers
```
docker ps
```
To list all the containers running or stop we use ```-a``` flag.
```
docker ps -a
```
---
### ❯ stop - stops the specied container
To use the stop command we either specify the container's name or id.  
i.e
```
docker stop <container name or id>
```
Enter the container name or id next to stop.

---
### ❯ start - starts the specied container
To use the stop command we either specify the container's name or id.  
i.e
```
docker start <container name or id>
```
Enter the container name or id next to stop.

---
### ❯ rm - removes the specied stopped container
This ```rm``` command removes the container.
```
docker rm <container name or id>
```
---
### ❯ images - displays list of downloaded images
The following command list images.
```
docker images
```
---
### ❯ rmi - removes the specified image
The following commands removes an image.
```
docker rmi <image name>
```
It is to be noted that the image must not be use by any containers.

---
### ❯ pull - downloads the specified image from docker hub
The following commands downloads an image from docker hub.
E.g
```
docker pull <image name>

### E.g docker pull nginx
```
---
### ❯ sleep - runs the container for specified time
```
docker run ubuntu sleep 5
```
This command runs ubuntu image and runs for 5 seconds and stops.

---

### ❯ exec - runs a command inside the container 
```
docker exec <container name or id> cat /etc/hosts
```
The above command execute ```cat /etc/hosts``` command in the specified container.

---

### ❯ attach and detach
To run container in the background we use ```-d``` flag.
```
docker run -d <image name>
```
To attach to the running container we use the following command.
```
docker attach <container name or id>
```
---

### ❯ inspect
To view more details of a docker container we use ```inspect``` command.
```
docker inspect <container name or id >
```
---

### ❯ logs
To view more details of a docker container we use ```inspect``` command.
```
docker log <container name or id>
```
---

‏‏‎ ‎
##  ❯ Run Command - details

### ❯ Version Tag
By default docker installs the latest versions of the image. If we want to install our own versions we specify a tag with the image name.
```
docker run redis:4.0
```
Any version type after this ```:``` colon will be downloaded in this case ```4.0```.

---
### ❯ -i and -it flag
```-i``` flag helps to run any application in an interactive mood especially associated with input purposes.  
If we attach ```t``` with ```-i``` this prompts the input more clearly.  
```
docker run -i <image>

## or

docker run -it <image>
```
**-it is more utilized**

---
### ❯ PORT Mapping
When we run a webapp inside docker , docker assigns an internal ip to it with the port of the appliaction.  
In order to access this webapp outside of docker we map the of our machine or pc to the port of the container running on docker.  
In this wasy we can easily view our webapp through our browser on our pc.
Example :
```
    localhost / our machine port
              ↓
docker run -p 80:8000 <image>
                  ↑
        docker container port
```
---
### ❯ Volume Mapping
If we want to store data of any container directory outside the container we use ```-v``` tag with the ```run``` command.  
Using this helps to store data locally if we delete our container our data is deleted. In order to save our data we store it locally.
```
        localhost / our machine storage
                   ↓  
docker run -v ${PWD}/mariadb:/var/lib/mysql 
                                ↑
                        docker container storage    
```
---
### ❯ Specify Container Name
To specify container name we have to use ```--name``` tag with run command.
```
                container name
                    ↓  
docker run --name ubuntu ubuntu
                            ↑
                        image name    
```
---

‏‏‎ ‎
## ❯ Accessing Web UI of a webapp
If we run a web based application and we need to see it's web UI there are total of two ways.
### ❯ Accessing UI through Docker's IP
If we use docker container IP and specify the port that is being used we can easily access the web UI of the running application in the container.  
To find docker container IP we have to use the following command.
```
             container name
                   ↓ 
$ docker inspect nginx | grep IP

            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2", ← This is the IP Addresss of the container.
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
                    "IPAMConfig": null,
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
```
This is from an nginx container that runs nginx service , which i ran through the following command.
```
                  container name
                       ↓ 
docker run -d --name nginx nginx
```
If we use the above ip with the port we can access the web UI.

---
### ❯ Accessing through port mapping
**!! See the port mapping section to map ports. !!**
After the port has been mapped we can use our localhost ip to access the web UI.
```
                        container name
                                ↓ 
docker run -p 80:80 -d --name nginx nginx
```
---






